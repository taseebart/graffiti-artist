class PaintTexture {
  constructor (options) {
    this.container = options.container
    this.renderer = options.renderer
    this.pixelRatio = options.pixelRatio || 2
    this.width = options.width * this.pixelRatio
    this.height = options.height * this.pixelRatio
    this.orthoCamera = options.orthoCamera
    this.brush = options.brush

    // Canvas 2D
    this.canvas = document.createElement('canvas')
    this.canvas.width = this.width
    this.canvas.height = this.height
    this.ctx = this.canvas.getContext('2d')

    // Canvas texture
    this.canvasMap = new THREE.CanvasTexture(this.canvas, THREE.UVMapping)
    this.canvasMap.minFilter = THREE.LinearFilter

    // State
    this.isDrawing = false
    this.timeSpraying = 0
  }

  getTexture () {
    return this.canvasMap
  }

  clear () {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.canvasMap.needsUpdate = true
  }

  update (paint, _x, _y) {
    const pr = this.pixelRatio
    const x = _x * pr
    const y = _y * pr

    if (paint) {
      if (!this.isDrawing) {
        this.lastPoint = { x, y }
        this.isDrawing = true
      }

      this.brush.paint(this.ctx, x, y, this.lastPoint, pr, this.timeSpraying)
      this.canvasMap.needsUpdate = true
      this.lastPoint = { x, y }
      this.timeSpraying++
    } else {
      this.timeSpraying = 0
      this.isDrawing = false
    }

    // Update brush cursor position
    this.brush.position(x, y, pr, this.timeSpraying)
  }
}

module.exports = PaintTexture
