uniform sampler2D background;
uniform sampler2D paint;

varying vec2 vUv;
// varying vec3 vNormal;
// varying vec3 vViewPosition;

void main() {
	vec4 bg = texture2D( background, vUv );
	vec4 p = texture2D( paint, vUv );

    // Limit paint alpha to show bump map
    float paintAlpha = min(p.a, 0.9);

    // Mix paint and background
    vec3 color = mix(bg.rgb, p.rgb, paintAlpha);

    gl_FragColor = vec4( color, 1.0 );
}
