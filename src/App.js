const OrbitControls = require('three-orbit-controls')(THREE)

const glslify = require('glslify')
const wallVert = glslify('./shaders/wall.vert')
const wallFrag = glslify('./shaders/wall.frag')

const BackgroundTexture = require('./BackgroundTexture')
const PaintTexture = require('./PaintTexture')
const Brush = require('./Brush')

// const events = require('./utils/events')
const UI = require('./utils/ui')
const stats = require('./utils/stats')

class App {
  constructor (options) {
    const defaults = {
      paused: false,
      pausedRenderer: false,
      useOrthoCamera: true,
      paintMode: false
    }
    this.state = Object.assign(defaults, options || {})
    // console.log(this.state)

    window.addEventListener('resize', this.onResize.bind(this), false)
    this.onResize()
  }

  set (key, value) {
    this.state[key] = value
  }

  get (key) {
    return this.state[key]
  }

  init3dScene () {
    this.scene = new THREE.Scene()

    this.initRenderer()

    this.initCameras()
    this.initOrbitControls()
    this.initBrush()
    this.initWall()
    this.initLights()

    // Time
    this.clock = new THREE.Clock()
  }

  initRenderer () {
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: false,
      canvas: document.getElementById('webgl-canvas')
    })
    this.gl = this.renderer.context

    this.renderer.setClearColor('#333333', 1)
    // this.renderer.setPixelRatio(1)
    this.renderer.setPixelRatio(window.devicePixelRatio || 1)
    this.renderer.shadowMap.enabled = false
    this.renderer.gammaInput = true
    this.renderer.gammaOutput = true
    this.renderer.physicallyCorrectLights = true

    this.renderer.setSize(this.width, this.height)
    document.body.appendChild(this.renderer.domElement)
  }

  /**
   * The 2 cameras: orthographic and perspective.
   * Perss 'O' to toggle cameras (see initKeyEvents() method).
   */
  initCameras () {
    // Orthographic camera
    this.orthoCamera = new THREE.OrthographicCamera(
      -this.width / 2,
      this.width / 2,
      this.height / 2,
      -this.height / 2,
      0,
      1
    )
    this.scene.add(this.orthoCamera)

    // Perspective camera
    this.camera = new THREE.PerspectiveCamera(50, this.width / this.height, 0.1, 10000)
    this.camera.position.x = 0 // o-this.width * 0.4
    this.camera.position.y = -this.height * 0.2
    this.camera.position.z = this.height * 2
    this.camera.lookAt(this.scene.position)
    this.scene.add(this.camera)
  }

  initOrbitControls () {
    this.controls = new OrbitControls(this.camera)
  }

  initBrush () {
    this.brush = new Brush({
      container: document.body,
      color: new THREE.Color(216, 39, 23), // #d82717
      size: 100,
      hardness: 0.15, // 0 to 1
      ui: this.ui
    })
  }

  setRandomBackgroundImage () {
    this.bgTex.setRandomImage()

    if (this.uniforms) {
      this.uniforms.background.value = this.bgTex.getTexture()
    }
  }

  initWall () {
    this.bgTex = new BackgroundTexture({
      images: this.state.assets,
      width: this.width,
      height: this.height,
      pixelRatio: 4,
      renderer: this.renderer,
      orthoCamera: this.orthoCamera
    })
    this.setRandomBackgroundImage()

    this.paintTex = new PaintTexture({
      width: this.width,
      height: this.height,
      pixelRatio: 4,
      renderer: this.renderer,
      orthoCamera: this.orthoCamera,
      brush: this.brush
    })

    // Wall shader material
    this.uniforms = {
      pointer: { value: new THREE.Vector2() },
      background: { value: this.bgTex.getTexture() },
      paint: { value: this.paintTex.getTexture() },
      time: { value: 1.0 }
    }
    var material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      side: THREE.DoubleSide,
      vertexShader: wallVert,
      fragmentShader: wallFrag
    })

    // Geometry
    const geometry = new THREE.PlaneBufferGeometry(this.width, this.height)

    // Wall mesh
    this.wall = new THREE.Mesh(geometry, material)

    this.scene.add(this.wall)
  }

  initLights () {
    this.ambientLight = new THREE.AmbientLight(0xffffff, 2)
    this.scene.add(this.ambientLight)

    this.pointLight = new THREE.PointLight(0xffffff, 1, 0, 1.5)
    this.pointLight.position.set(-2, 2, 5)
    this.scene.add(this.pointLight)
  }

  onResize () {
    const w = window.innerWidth
    const h = window.innerHeight

    this.width = w
    this.height = h
    this.set('width', w)
    this.set('height', h)

    if (this.renderer && this.camera) {
      this.camera.aspect = w / h
      this.camera.updateProjectionMatrix()
      this.renderer.setPixelRatio(1)
      // this.renderer.setPixelRatio(window.devicePixelRatio || 1)
      this.renderer.setSize(w, h)
    }

    // Graffiti UI
    if (this.ui) {
      this.updateGraffitiUiSize()
    }
  }

  updateGraffitiUiSize () {
    const graffitiUi = document.getElementById('graffiti-artist-ui')
    const graffitiRect = graffitiUi.getBoundingClientRect()
    this.graffitiUiWidth = graffitiRect.width
  }

  initRaycaster () {
    this.raycaster = new THREE.Raycaster()
    this.intersection = new THREE.Vector3()
  }

  updateRaycaster () {
    const camera = this.state.useOrthoCamera ? this.orthoCamera : this.camera
    this.raycaster.setFromCamera(this.pointer, camera)

    // console.log(this.pointer.x, this.pointer.y, camera)

    const intersections = this.raycaster.intersectObject(this.wall)

    if (intersections && intersections[0]) {
      this.intersection = intersections[0].point

      this.paintUV = intersections[0].uv
      this.paintX = this.paintUV.x * this.width
      this.paintY = this.height - this.paintUV.y * this.height

      // console.log(this.paintX, this.paintY)
    }
  }

  toggleCamera () {
    this.pointerDown = false
    this.disablePointer = false
    this.state.useOrthoCamera = !this.state.useOrthoCamera

    if (this.state.useOrthoCamera) {
      this.brush.show()
    } else {
      this.brush.hide()
    }

    this.ui.updateCamerasUI(this.state.useOrthoCamera)
  }

  initPointerEvents () {
    const el = document.body

    this.updateGraffitiUiSize()

    this.pointer = new THREE.Vector2(Math.random() * 1 - 0.5, Math.random() * 1 - 0.5)

    // const pointerEvent = isMobile.any ? 'touchstart' : 'mousemove'

    const getPointerPosition = e => {
      const x = e.clientX / this.width * 2 - 1
      const y = -(e.clientY / this.height) * 2 + 1
      return { x, y }
    }
    const start = e => {
      if (this.disablePointer) return

      // Check if UI is covering the area
      const pos = getPointerPosition(e)
      const x = (pos.x + 1) / 2 * this.width
      const visibleArea = this.width - this.graffitiUiWidth
      const isVisible = !this.ui.active || x < visibleArea

      // if (this.state.useOrthoCamera && isVisible) {
      //   this.pointerDown = true
      // }

      if (isVisible) {
        this.pointerDown = true
      }
    }
    const stop = () => {
      this.pointerDown = false
    }
    const move = e => {
      const pos = getPointerPosition(e)
      this.pointer.set(pos.x, pos.y)
    }

    // Mouse events
    el.addEventListener('mousemove', e => move(e))
    el.addEventListener('mousedown', e => start(e))
    el.addEventListener('mouseup', e => stop(e))

    // Touch events
    el.addEventListener('touchmove', event => {
      const e = event.targetTouches[0]
      move(e)
    })
    el.addEventListener('touchstart', event => {
      const e = event.targetTouches[0]
      start(e)
    })
    el.addEventListener('touchend', event => {
      const e = event.targetTouches[0]
      stop(e)
    })
  }

  initWheelEvents () {
    const wheel =
      'onwheel' in document.createElement('div')
        ? 'wheel' // Modern browsers support "wheel"
        : document.onmousewheel !== undefined
          ? 'mousewheel' // Webkit and IE support at least "mousewheel"
          : 'DOMMouseScroll' // let's assume that remaining browsers are older Firefox

    document.body.addEventListener(wheel, e => {
      if (this.controls.enabled) return

      const delta = Math.min(10, Math.max(-10, e.deltaY / 100))
      this.brush.updateSize(delta)
    })
  }

  update () {
    stats.begin()

    this.delta = Math.min(this.clock.getDelta() * 4, 1) // safety cap on large deltas
    this.time = this.clock.elapsedTime
    this.render(this.time, this.delta)

    stats.end()

    window.requestAnimationFrame(this.update.bind(this))
  }

  render (time, delta) {
    // if (!this.state.paused) {
    //   this.uniforms.time.value = time
    // }

    if (!this.state.pausedRenderer) {
      this.updateRaycaster()

      // Update paint texture
      this.paintTex.update(this.pointerDown, this.paintX, this.paintY)

      if (this.pointerDown) {
        this.uniforms.paint.value = this.paintTex.getTexture()
      }

      // Render
      if (!this.state.useOrthoCamera) {
        this.controls.enabled = !this.state.paintMode
        this.disablePointer = !this.state.paintMode
        this.renderer.render(this.scene, this.camera)
      } else {
        this.controls.enabled = false
        this.renderer.render(this.scene, this.orthoCamera)
      }
    }
  }

  initUI () {
    this.ui = new UI({
      isMobile: this.get('isMobile'),
      debug: this.get('debug'),
      events: {
        overBtn: () => {
          this.disablePointer = true
        },
        outBtn: () => {
          this.disablePointer = false
        },
        clearBtn: () => {
          this.pointerDown = false
          this.paintTex.clear()
        },
        cameraBtn: () => {
          this.toggleCamera()
          return this.state.useOrthoCamera
        },
        onHardnessSlider: e => {
          const val = parseFloat(e[0])
          this.brush.setHardness(val)
        },
        onSizeSlider: e => {
          const val = parseInt(e[0])
          this.brush.setSize(val * 2)
        },
        onChangeColor: c => {
          const color = new THREE.Color(c.r, c.g, c.b)
          this.brush.setColor(color)
        },
        cameraOrtho: () => {},
        cameraPerspective: () => {},
        paintMode: paint => {
          this.state.paintMode = paint
        },
        bgBtn: () => {
          this.setRandomBackgroundImage()
        }
      }
    })
    this.ui.appendStats(stats.dom)
    this.ui.updateCamerasUI(this.state.useOrthoCamera)
    this.ui.updatePaintMode(this.state.paintMode)
  }

  initKeyEvents () {
    document.body.addEventListener('keyup', e => {
      if (e.key === 'o' || e.key === 'O') {
        this.toggleCamera()
      } else if (e.key === 'c' || e.key === 'C') {
        const r = ~~(Math.random() * 255)
        const g = ~~(Math.random() * 255)
        const b = ~~(Math.random() * 255)
        this.brush.setColor(new THREE.Color(r, g, b))
      } else if (e.key === 'v' || e.key === 'V') {
        // Toggle view or paint mode
        this.state.paintMode = !this.state.paintMode
        this.ui.updatePaintMode(this.state.paintMode)
      } else if (e.key === 'h' || e.key === 'H') {
        // Toggle all UI + target + grid
        document.body.classList.toggle('show-ui-btn')
      } else if (e.key === 'p' || e.key === 'P') {
        // Pause animations
        const pausedState = this.get('paused')
        this.set('paused', !pausedState)
      } else if (e.key === 'b' || e.key === 'B') {
        // Change wall background
        this.setRandomBackgroundImage()
      }
    })
  }

  start () {
    this.initUI()
    this.init3dScene()
    this.initPointerEvents()
    this.initWheelEvents()
    this.initRaycaster()
    this.initKeyEvents()

    this.update()

    setTimeout(() => this.ui.removeLoader(), 500)
  }
}

module.exports = App
