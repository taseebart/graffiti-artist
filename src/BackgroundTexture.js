class BackgroundTexture {
  constructor (options) {
    this.renderer = options.renderer
    this.pixelRatio = options.pixelRatio || 2
    this.width = options.width * this.pixelRatio
    this.height = options.height * this.pixelRatio
    this.orthoCamera = options.orthoCamera
    this.images = options.images

    // Create a different scene to hold our buffer objects
    this.bufferScene = new THREE.Scene()
    // Create the texture that will store our result
    this.rt = new THREE.WebGLRenderTarget(this.width, this.height, {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.NearestFilter
    })

    this.drawScene()
  }

  drawScene () {
    const geometry = new THREE.PlaneBufferGeometry(this.width, this.height)
    this.material = new THREE.MeshStandardMaterial({
      bumpScale: 1.5,
      metalness: 0,
      roughness: 1,
      emissive: 0x000000
    })

    // Lights
    this.ambientLight = new THREE.AmbientLight(0xffffff, 2)
    this.bufferScene.add(this.ambientLight)

    this.pointLight = new THREE.PointLight(0xffffff, 4, 0, 2)
    this.pointLight.position.set(0, 0, 200)
    this.bufferScene.add(this.pointLight)

    // Wall mesh
    this.wall = new THREE.Mesh(geometry, this.material)
    this.bufferScene.add(this.wall)
  }

  getRandomImage () {
    const images = [
      {
        map: this.images['img/bricks-map.jpg'],
        bumpMap: this.images['img/bricks-bump-map.jpg'],
        mapSizeX: 512 / 2,
        mapSizeY: 512 / 2
      },
      {
        map: this.images['img/stones-map.jpg'],
        bumpMap: this.images['img/stones-bump-map.jpg'],
        mapSizeX: 1024 / 4,
        mapSizeY: 1024 / 4
      },
      {
        map: this.images['img/concrete1-map.jpg'],
        bumpMap: this.images['img/concrete1-bump-map.jpg'],
        mapSizeX: 828 / 2,
        mapSizeY: 598 / 2
      },
      {
        map: this.images['img/concrete2-map.jpg'],
        bumpMap: null,
        mapSizeX: 1500 / 2,
        mapSizeY: 1500 / 2
      }
    ]
    const randomIdx = Math.floor(Math.random() * images.length)

    // Make sure image is always differnt from the previous
    if (
      (!this.currentImageIdx && this.currentImageIdx !== 0) ||
      this.currentImageIdx !== randomIdx
    ) {
      this.currentImageIdx = randomIdx
      return images[randomIdx]
    } else {
      return this.getRandomImage()
    }
  }

  setRandomImage () {
    const image = this.getRandomImage()
    this.updateMaps(image)
  }

  updateMaps (options) {
    this.map = options.map
    this.bumpMap = options.bumpMap
    this.mapSizeX = options.mapSizeX
    this.mapSizeY = options.mapSizeY

    const map = this.map
    const bumpMap = this.bumpMap

    // Repeat texture
    const repeatX = Math.ceil(this.width / this.mapSizeX)
    const repeatY = Math.ceil(this.height / this.mapSizeY)

    if (map) {
      map.minFilter = THREE.LinearFilter
      map.magFilter = THREE.NearestFilter
      map.repeat.x = repeatX
      map.repeat.y = repeatY
      map.wrapS = THREE.RepeatWrapping
      map.wrapT = THREE.RepeatWrapping
      this.material.map = map
    }

    if (bumpMap) {
      bumpMap.minFilter = THREE.LinearFilter
      bumpMap.magFilter = THREE.NearestFilter
      bumpMap.repeat.x = repeatX
      bumpMap.repeat.y = repeatY
      bumpMap.wrapS = THREE.RepeatWrapping
      bumpMap.wrapT = THREE.RepeatWrapping
      this.material.bumpMap = bumpMap
    }

    this.material.needsUpdate = true
    this.update()
  }

  getTexture () {
    return this.rt.texture
  }

  update () {
    this.renderer.render(this.bufferScene, this.orthoCamera, this.rt)
  }
}

module.exports = BackgroundTexture
