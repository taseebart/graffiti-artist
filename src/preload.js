function preload (options) {
  const onComplete = options.onComplete
  const texturesUrls = [
    'img/bricks-map.jpg',
    'img/bricks-bump-map.jpg',
    'img/stones-map.jpg',
    'img/stones-bump-map.jpg',
    'img/concrete1-map.jpg',
    'img/concrete2-map.jpg'
  ]
  const loadedTextures = {}

  // Manager
  const manager = new THREE.LoadingManager()
  manager.onStart = (url, itemsLoaded, itemsTotal) => {
    // console.log(
    //   'Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.'
    // )
  }

  manager.onLoad = () => {
    console.log('Preload complete')
    onComplete(loadedTextures)
  }

  manager.onProgress = (url, itemsLoaded, itemsTotal) => {
    console.log(
      'Loading: ' + url + ' (' + itemsLoaded + '/' + itemsTotal + ')'
    )
  }

  manager.onError = url => {
    console.log('There was an error loading ' + url)
  }

  // Load all textures
  const loader = new THREE.TextureLoader(manager)

  texturesUrls.forEach(url => {
    loader.load(url, t => {
      loadedTextures[url] = t

      // console.log(t)
    })
  })
}

module.exports = preload
