const noUiSlider = require('nouislider')
const $ = require('jquery')
require('spectrum-colorpicker')

const html = `
<div id="ui-btn" class="btn">
    <span class="show">Options</span>
    <span class="hide">Hide Options</span>
</div>

<div id="ui" class="show">
  <div id="graffiti-artist-ui">
    <div class="panel">

        <div id="brush-settings" class="options-item">
          <div class="options-item">
            <h2>Spray Color</h2>
            <p>Press C to get a random color ot click to select</p>
            <div class="brush-color-wrapper">
              <div id="brush-color" class="brush"></div>
            </div>
          </div>

          <div class="options-item">
            <h2>Spray distance (size)</h2>
            <p>You can use the mousewheel too!</p>
            <div class="brush-size-wrapper">
              <div id="brush-size" class="brush">
                <span id="brush-size-value" class="center-in-circle"></span>
              </div>
              <div class="input-wrapper">
                <div id="brush-size-range"></div>
              </div>
            </div>
          </div>

          <div class="options-item">
            <h2>Spray amount</h2>
            <p>Brush hardness</p>
            <div class="brush-hardness-wrapper">
              <div id="brush-hardness" class="brush">
                <!--<span id="brush-hardness-value" class="center-in-circle"></span>-->
              </div>
              <div class="input-wrapper">
                <div id="brush-hardness-range"></div>
              </div>
            </div>
          </div>
        </div>

        <div id="buttons" class="options-item">

          <div class="btn-wrapper">
            <p>Try painting in perspective view!<br></p>
            <div id="camera-btn" class="btn">Switch to Perspective View</div>
          </div>

          <div id="paint-view-mode" class="modes-group">
          <p><strong>Paint mode</strong> to paint and <strong>View mode</strong> to navigate in 3D!</p>
            <label class="radio-container">
              View mode
              <input id="view-mode" type="radio" name="radio">
              <span class="checkmark"></span>
            </label>
            <label class="radio-container">
              Paint mode
              <input id="paint-mode" type="radio" name="radio">
              <span class="checkmark"></span>
            </label>
          </div>

          <div class="btn-wrapper">
            <p>Try another background!</p>
            <div id="bg-btn" class="btn">Random wall background</div>
          </div>

          <div class="btn-wrapper">
            <div id="clear-btn" class="btn">Clear</div>
          </div>
        </div>

        <!-- <div id="modes" class="options-item"></div> -->

    </div>
  </div>


  <div class="info">
    <h3>Graffiti Artist</h3>
    <p>
    <span class="credits">Code: Esteban Almiron</span>
    </p>
  </div>

</div>

`

const defaults = {
  isMobile: false
}

class ui {
  constructor (options) {
    const body = document.body
    this.options = Object.assign(defaults, options || {})

    // Events
    this.events = options.events

    // Show loader + mobile support
    body.classList.add('show-loader')
    body.classList.add(this.options.isMobile ? 'mobile' : 'no-mobile')

    // Prepend html to body
    body.insertAdjacentHTML('afterbegin', html)

    // Setup UI options
    this.brushColor = document.getElementById('brush-color')
    this.brushSize = document.getElementById('brush-size')
    this.brushSizeValue = document.getElementById('brush-size-value')
    this.brushHardness = document.getElementById('brush-hardness')
    this.brushHardnessValue = document.getElementById('brush-hardness-value')

    // Buttons
    this.cameraBtn = document.getElementById('camera-btn')
    this.bgBtn = document.getElementById('bg-btn')
    this.clearBtn = document.getElementById('clear-btn')
    this.uiBtn = document.getElementById('ui-btn')

    // Modes
    this.paintViewMode = document.getElementById('paint-view-mode')
    this.paintMode = document.getElementById('paint-mode')
    this.viewMode = document.getElementById('view-mode')

    // Setup custom range sliders
    this.setupRangeSliders()

    // Setup color picker
    this.setupColorpicker()

    // Setup ui events
    this.setupButtons()

    // Setup modes
    this.setupModes()

    // State
    this.active = false
  }

  setupColorpicker () {
    this.colorpicker = $('#brush-color').spectrum({
      showButtons: false
    })

    this.colorpicker.on('move.spectrum', (e, tinycolor) => {
      this.events.onChangeColor(tinycolor.toRgb())
      // console.log(tinycolor.toRgb())
      // console.log(tinycolor.toHexString())
    })
  }

  setupRangeSliders () {
    this.sizeSlider = document.getElementById('brush-size-range')
    this.hardnessSlider = document.getElementById('brush-hardness-range')

    noUiSlider.create(this.sizeSlider, {
      start: [0],
      step: 1,
      connect: [true, false],
      range: {
        min: [0],
        max: [150]
      }
    })

    noUiSlider.create(this.hardnessSlider, {
      start: [0.5],
      step: 0.05,
      connect: [true, false],
      range: {
        min: [0.05],
        max: [0.8]
      }
    })

    this.sizeSlider.noUiSlider.on('change', this.events.onSizeSlider)
    this.hardnessSlider.noUiSlider.on('change', this.events.onHardnessSlider)
  }

  setupButtons () {
    const body = document.body

    const click = this.options.isMobile ? 'touchstart' : 'click'

    // Open options button
    this.uiBtn.addEventListener(click, e => {
      e.stopPropagation()

      this.active = !this.active

      if (this.active) {
        // this.events.activate()
        body.classList.add('show-ui')
      } else {
        // this.events.deactivate()
        body.classList.remove('show-ui')
      }
    })

    this.uiBtn.addEventListener('mouseover', e => {
      this.events.overBtn()
    })
    this.uiBtn.addEventListener('mouseout', e => {
      this.events.outBtn()
    })

    // Clear button
    this.clearBtn.addEventListener(click, e => {
      // console.log(e)
      this.events.clearBtn()
    })

    // Camera button
    this.cameraBtn.addEventListener(click, e => {
      const ortho = this.events.cameraBtn()

      this.updateCamerasUI(ortho)
    })

    this.bgBtn.addEventListener(click, e => {
      this.events.bgBtn()
    })
  }

  setupModes () {
    $('#paint-view-mode').find('.radio-container input').on('change', e => {
      const paintMode = e.target.id === 'paint-mode' && e.target.checked
      this.events.paintMode(paintMode)
    })
  }

  updatePaintMode (paintMode) {
    if (paintMode) {
      this.paintMode.checked = true
    } else {
      this.viewMode.checked = true
    }
  }

  updateCamerasUI (ortho) {
    if (ortho) {
      this.events.cameraOrtho()
      this.cameraBtn.innerHTML = 'Switch to Perspective View'
      this.paintViewMode.style.display = 'none'
    } else {
      this.events.cameraPerspective()
      this.cameraBtn.innerHTML = 'Switch to Orthographic View'
      this.paintViewMode.style.display = 'block'
    }
  }

  appendStats (statsDom) {
    if (statsDom) {
      document.getElementById('ui').appendChild(statsDom)
    }

    return this
  }

  removeLoader () {
    document.body.classList.remove('show-loader')
    document.body.classList.add('show-ui-btn')

    return this
  }
}

module.exports = ui
