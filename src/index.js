require('babel-polyfill')
const App = require('./App')
const ismobilejs = require('ismobilejs')
const preload = require('./preload')

function init (assets) {
  // DEBUG
  const debug = true

  // Support
  const isMobile = ismobilejs.any
  console.log('isMobile', isMobile)

  // Launch the app
  const app = new App({
    debug,
    assets
  })

  // Start demo
  app.start()

  // expose for debug
  window.app = app
}

preload({
  onComplete: assets => init(assets)
})
