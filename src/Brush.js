class Brush {
  constructor (options) {
    this.color = this.colorToString(options.color) || '255, 0, 0'
    this.size = options.size || 20
    this.hardness = options.hardness || 0.5
    this.container = options.container
    this.ui = options.ui

    // Settings
    this.minSize = 8
    this.maxSize = 300
    this.minHardness = 0.05
    this.maxHardness = 0.8

    // Create brush view
    this.createCursor()

    // Update UI
    this.updateUI()
  }

  createCursor () {
    this.el = document.createElement('div')
    this.container.appendChild(this.el)
    TweenMax.set(this.el, {
      x: 100,
      y: 100,
      width: this.size / 2,
      height: this.size / 2,
      border: `2px dashed rgb(${this.color})`,
      opacity: 1,
      borderRadius: '50%',
      zIndex: 3,
      left: 0,
      top: 0,
      position: 'absolute'
    })
  }

  colorToString (color) {
    return `${color.r}, ${color.g}, ${color.b}`
  }

  setColor (color) {
    if (color.isColor) {
      this.color = this.colorToString(color)
    } else {
      this.color = color
    }

    TweenMax.set(this.el, {
      borderColor: `rgb(${this.color})`
    })

    TweenMax.set(this.ui.brushColor, {
      backgroundColor: `rgb(${this.color})`
    })

    this.ui.colorpicker.spectrum('set', `rgba(${this.color}, 1)`)
  }

  updateSize (val) {
    if (!val) return

    this.size = Math.min(this.maxSize, Math.max(this.minSize, this.size + Math.floor(val)))

    this.updateSizeUI(Math.floor(this.size / 2))
  }

  setSize (val) {
    if (!val) return

    this.size = Math.min(this.maxSize, Math.max(this.minSize, val))

    this.updateSizeUI(Math.floor(this.size / 2))
  }

  updateSizeUI (val) {
    this.ui.brushSizeValue.innerText = val
    this.ui.sizeSlider.noUiSlider.set(val)

    TweenMax.set(this.el, {
      width: this.size / 2,
      height: this.size / 2
    })
  }

  updateHardnessUI (val) {
    // this.ui.brushHardnessValue.innerText = val
    this.ui.hardnessSlider.noUiSlider.set(val)

    TweenMax.set(this.ui.brushHardness, {
      background: `radial-gradient(
        ellipse at center,
        rgba(255, 255, 255, 1) ${Math.floor(val * 100)}%,
        rgba(255, 255, 255, 0) 100%
      )`
    })
  }

  setHardness (val) {
    if (!val && val !== 0) return

    this.hardness = val

    this.updateHardnessUI(val)
  }

  distance (a, b) {
    return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2))
  }

  angleBetween (a, b) {
    return Math.atan2(b.x - a.x, b.y - a.y)
  }

  circle (x, y, radius, color, hardness, ctx) {
    const opacity = Math.min(Math.max(hardness, this.minHardness), this.maxHardness)

    ctx.beginPath()
    var gradient = ctx.createRadialGradient(x, y, 0, x, y, radius)
    gradient.addColorStop(0, `rgba(${color}, ${opacity})`)
    gradient.addColorStop(hardness, `rgba(${color}, ${opacity})`)
    gradient.addColorStop(1, `rgba(${color}, 0)`)
    ctx.fillStyle = gradient
    ctx.arc(x, y, radius, 0, Math.PI * 2)
    ctx.fill()
    ctx.closePath()
  }

  /**
   * To create a continuous line calculate distance from last point
   * and fill the gap with circles
   * (instead of simply drawing a circle on the current x/y coord)
   */
  paint (ctx, x, y, lastPoint, pixelRatio, timeSpraying) {
    // Continuous smooth line made of circles
    const dist = this.distance(lastPoint, { x, y })
    const angle = this.angleBetween(lastPoint, { x, y })

    for (let i = 0; i < dist + 1; i += this.size / 4) {
      // interpolated x and y
      const ix = lastPoint.x + Math.sin(angle) * i
      const iy = lastPoint.y + Math.cos(angle) * i

      this.circle(ix, iy, this.size, this.color, this.hardness, ctx)
    }

    // Alternative method: simply draw a circle on current x/y coords
    // this.circle(x, y, this.size, this.color, this.ctx)
  }

  /**
   * Update brush cursor position
   */
  position (x, y, pixelRatio) {
    const domX = x / pixelRatio
    const domY = y / pixelRatio
    const offset = this.size / 4 + 2
    TweenMax.set(this.el, { x: domX - offset, y: domY - offset })
  }

  show () {
    this.el.style.display = 'block'
  }

  hide () {
    this.el.style.display = 'none'
  }

  updateUI () {
    this.setSize(this.size)
    this.setColor(this.color)
    this.setHardness(this.hardness)
  }
}

module.exports = Brush
