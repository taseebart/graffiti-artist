# Graffiti Artist

[Demo](http://graffiti-artist.surge.sh/)

***NOTE: because of the limited time, this demo is not fully responsive (ie. it does not adapt automatically when the page gets resized).
Please let me know if you want to see that feature implemented, I can add it.***

### Setup

`npm install` or `yarn install`

### Run

`npm start` or `yarn start`

A server should be running on port 9966: `http://localhost:9966`

### How to use

Click the `OPTIONS` button to... see the options panel.

Use the mousesheel to change the size of the brush.

Press `O` key to to toggle orthographic and perspective cameras.

Press `V` key to toggle view or paint mode (only in perspective view).

Press `C` key to choose a random color.

Press `B` key to set a random background.
